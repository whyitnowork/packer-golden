.PHONY: pkr-deb11-esxi
.PHONY: pkr-deb11-pve

pkr-deb11-esxi:
	packer build -force -var-file=debian11/esxi-vars.json debian11/esxi-deb11.json

pkr-deb11-pve:
	packer build -var-file=debian11/pve-vars.json debian11/pve-deb11.json
